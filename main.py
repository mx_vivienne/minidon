#!/usr/bin/python3

import argparse

from api import *
from parser import *

def get_acc(accounts, alias):
    for acc in accounts:
        if (alias == acc.alias):
            return acc
    return None

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest="mode", required=True)

# parsers:

# new account
# minidon new <alias> <instance url> <email>
parser_new = subparsers.add_parser("new", help="Add a new account.")
parser_new.add_argument("alias", type=str, 
        help="A unique name for the new account, it is recommended to pick a short one.")
parser_new.add_argument("instance", type=str,
        help="URL of the instance the account is on.")
parser_new.add_argument("email", type=str,
        help="The email address associated with the new account.")

# list accounts
# minidon list
parser_list = subparsers.add_parser("list", help="List all known accounts by their aliases.")

# forget account
# minidon forget <alias>
parser_forget = subparsers.add_parser("remove", help="Forget an account.")
parser_forget.add_argument("alias", type=str,
        help="The alias that was defined for this account. Use `minidon list` to see all known accounts.")

# make post
# minidon post <alias> <content> [-w CW] [-v VISIBILITY]
parser_post = subparsers.add_parser("post", help="Send a post.")
parser_post.add_argument("alias", type=str,
        help="The account to post with.")
parser_post.add_argument("content", type=str,
        help="The post to send.")
parser_post.add_argument("-w", "--warning", type=str, dest="cw", nargs="?",
        help="An optional content warning")
parser_post.add_argument("-v", "--visibility", type=str, nargs="?",
        choices=["public", "unlisted", "private", "direct"],
        help="The visibility to set. Uses the account's default if not specified.")


# parse and do the stuff
args = parser.parse_args()

# make sure config file and parent directories exist
CONF_DIR.mkdir(parents=True, exist_ok=True)
DATA_FILE.touch()

# get accounts
accounts = AccountData.read_all()


# add account
if ("new" == args.mode):
    if [a for a in accounts if (args.alias == a.alias)]:
        print("This alias is already in use.")

    else:
        acc = AccountData(args.alias, args.instance, args.email)
        acc.register()
        print("Please go to this URL and login:")
        print(acc.get_auth_url())
        print("\nThen paste the authentication code here:")
        acc.add_auth_code( input("> ") )
        acc.login()
        accounts.append(acc)
        AccountData.write_all(accounts)
        print("Account successfully added!")


# list known accounts
elif ("list" == args.mode):
    for acc in accounts:
        acc.get_user()
        print(acc.alias + ":")
        print("    " + acc.user.username + " on " + acc.instance + "\n")


# forget an account
elif ("forget" == args.mode):
    acc = get_acc(accounts, args.alias)
    if (acc):
        print("Forget this account? (y/n)")
        if ("y" == input("> ")):
            accounts.remove(acc)
            print("Account forgotten.")
        else:
            print("Aborted.")

    else:
        print("No account with alias {0} found.".format(args.alias))


# make post
elif ("post" == args.mode):
    acc = get_acc(accounts, args.alias)
    if (acc):
        acc.api.status_post(
                status       = args.content, 
                visibility   = args.visibility,
                spoiler_text = args.cw)
    else:
        print("No account with alias {0} found.".format(args.alias))
