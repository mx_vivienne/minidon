from mastodon import Mastodon, StreamListener
from pathlib import Path
from json.decoder import JSONDecodeError
import json

APP_NAME = "Minidon"
CONF_DIR = Path("~/.config/minidon").expanduser()
DATA_FILE = CONF_DIR / "data.json"

class NotificationListener(StreamListener):
    def on_notification(notification):
        pass

class AccountData():
    def __init__(self, alias, instance, email=None):
        self.instance  = instance
        self.email     = email

        self.auth_code = None
        self.alias     = alias
        self.file      = CONF_DIR / (alias + ".secret")
        self.api       = None
        self.user      = None

        self.usable    = False

    def create_api(self):
        self.api = Mastodon(
                access_token  = self.file,
                api_base_url  = self.instance)

    def register(self):
        Mastodon.create_app(APP_NAME, to_file=self.file, api_base_url=self.instance)
        self.api = Mastodon(
                client_id    = self.file,
                api_base_url = self.instance)

    def get_auth_url(self):
        return self.api.auth_request_url(client_id=self.file)

    def add_auth_code(self, code):
        self.auth_code = code

    def login(self):
        self.api.log_in(username=self.email, code=self.auth_code, to_file=self.file)
        self.usable = True

    def read_all():
        with open(DATA_FILE, "r") as f:
            accounts = []
            try:
                for x in json.load(f):
                    acc = AccountData(x["alias"], x["instance"])
                    acc.create_api()
                    accounts.append(acc)
            except JSONDecodeError:
                pass
            return accounts

    def write_all(accounts):
        with open(DATA_FILE, "w") as f:
            json.dump([{"instance": a.instance, "alias": a.alias} for a in accounts], f)

    def get_user(self):
        self.user = self.api.me()

### adding new account:
# acc = AccountData(filename, url, email)
# acc.register()
# print(acc.get_auth_url)
# acc.add_auth_code(code)
# acc.login()

