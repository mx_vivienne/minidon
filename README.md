# Minidon

A Mastodon client to post from your terminal, supporting any number of accounts.

# Dependencies

Mastodon.py

# Usage

First, add an account with the `new` command, giving it an alias:\
`./main.py new (alias) (instance url) (your email)`\
Now a link to your instance will be displayed, where you will be asked to authorize this client.
It will then give you the key that Minidon needs to be able to post.

From then on, you can send posts with the `post` command, selecting an account with the alias assigned to it:\
`./main.py post (alias) (post content)`\
Content warnings can be set with `-w (text)`, and the post visibility is set with `-v (public/unlisted/private/direct)`

Registered accounts can be listed with `./main.py list` or removed with `./main.py remove (alias)`
